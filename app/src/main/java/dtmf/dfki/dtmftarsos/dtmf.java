package dtmf.dfki.dtmftarsos;

import java.sql.Array;

/**
 * Created by asingh on 9/5/14.
 */
public class dtmf {
}


public class DTMFprocessor
{

    /*
       The following constants are used to find the DTMF tones.  The COL1 is the column Hz that we
       are searching for, and the COL2 is the row Hz.  The DTMF_LAYOUT is the order that the cols and
       rows intersect at.
    */

    private static   int [] COL1 = [697, 770, 852, 941];
    private static final int[]    COL2= [1209, 1336, 1477, 1633];
    private static String[][]  DTMF_LAYOUT= [ ["1","2","3","A"] ,
                                      ["4","5","6","B"] ,
                                     ["7","8","9","C"] ,
                                    ["*","0","#","D"] ];

    private int sampleRate;
    private String lastFound = "";

    public int DTMFToneSensitivity = 15;

    /**
     * DTMF Processor Constructor
     *
     * @param sampleRate  This is the sample rate, in frames per second that the application is operating in.
     *
     */
    int sampleRate = 44100;
    public   DTMFprocessor(int sampleRate)
    {
        this.sampleRate = sampleRate;
    }



    /**
     * Searches a ByteArray (from a Sound() object) for a valid DTMF tone.
     *
     * @param tone  ByteArray that should contain a valid DTMF tone.
     * @return      string representation of DTMF tones.  Will return a blank string ('') if nothing is found
     *
     */
    public   String searchDTMF(ByteArray tone)
    {
        int position = 0;
        String charFound = "";

        // seed blank values for strongest tone to be found in the byteArray
        int maxCol  = -1;
        int maxColValue = 0;
        int maxRow  = -1;
        int maxRowValue = 0;
        String foundTones  = "";

        // reset the byteArray to the beginning, should we have gotten it in any other state.
        tone.position = position;

        // break up the byteArray in manageable chunks of 8192 bytes.
        for (int bufferLoop =0; bufferLoop < tone.bytesAvailable; bufferLoop =+ 8192)
        {
            position = bufferLoop;

            // search for the column tone.
            for (var col:int = 0; col < 4; col++)
            {
                if (powerDB(goertzel(tone,8192,COL1[col],position)) > maxColValue)
                {
                    maxColValue = powerDB(goertzel(tone,8192,COL1[col],position));
                    maxCol = col;
                }
            }

            // search for the row tone.
            for (var row:int = 0; row < 4; row++)
            {
                if (powerDB(goertzel(tone,8192,COL2[row],position)) > maxRowValue)
                {
                    maxRowValue = powerDB(goertzel(tone,8192,COL2[row],position));
                    maxRow = row;
                }
            }

            // was there enough strength in both the column and row to be valid?
            if ((maxColValue < DTMFToneSensitivity) || (maxRowValue < DTMFToneSensitivity))
            {
                charFound = "";
            }
            else
            {
                charFound = DTMF_LAYOUT[maxCol][maxRow];
            }

            if (lastFound != charFound)
            {
                trace("Found DTMF Tone:",charFound);
                lastFound = charFound;  // this is so we don't have duplicates.
                foundTones = foundTones + lastFound;
            }

        }
        return foundTones;
    }

    /**
     * Converts amplitude to dB (power).
     *
     * @param value  amplitude value
     * @return       dB
     *
     */
    private float powerDB(float value)
    {
        return 20 * Math.log(Math.abs(value))*Math.LOG10E;
    }

    /**
     * This function returns the amplitude of the a seeked wave in the buffer.
     *
     * @param buffer      the byteArray that is being searched.
     * @param bufferSize  the size of the buffer that we wish to search.
     * @param frequency   the frequency (in Hz) that we are searching for.
     * @param bufferPos   the starting point that we want to search from.
     * @return            amplitude of the searched frequency, if any.
     *
     */
    private function goertzel(buffer:ByteArray, bufferSize:int, frequency:int, bufferPos:int):Number
    {
        float skn = 0;
        float skn1 = 0;
        float skn2 = 0;

        buffer.position = bufferPos;

        for (var i:int=0; i < bufferSize; i++)
        {
            skn2 = skn1;
            skn1 = skn;
            if (buffer.bytesAvailable > 0)
                skn = 2 * Math.cos(2 * Math.PI * frequency / sampleRate) * skn1 - skn2 + buffer.readFloat();
        }

        float wnk = Math.exp(-2 * Math.PI * frequency / sampleRate);

        return (skn - wnk * skn1);
    }

    /**
     * Returns the Hz of the string tone that is searched.
     *
     * @param tone  character that is being search for
     * @return      an untyped object that has col and row properties that contain the Hz of the DTMF tone.
     *
     */
    private function findDTMF(String tone)
    {
        var myDTMF:Object = new Object();

        switch(tone)
        {
            case "1":
                myDTMF.col = COL1[0];
                myDTMF.row = COL2[0];
                break;
            case "2":
                myDTMF.col = COL1[0];
                myDTMF.row = COL2[1];
                break;
            case "3":
                myDTMF.col = COL1[0];
                myDTMF.row = COL2[2];
                break;
            case "A":
                myDTMF.col = COL1[0];
                myDTMF.row = COL2[3];
                break;
            case "4":
                myDTMF.col = COL1[1];
                myDTMF.row = COL2[0];
                break;
            case "5":
                myDTMF.col = COL1[1];
                myDTMF.row = COL2[1];
                break;
            case "6":
                myDTMF.col = COL1[1];
                myDTMF.row = COL2[2];
                break;
            case "B":
                myDTMF.col = COL1[1];
                myDTMF.row = COL2[3];
                break;
            case "7":
                myDTMF.col = COL1[2];
                myDTMF.row = COL2[0];
                break;
            case "8":
                myDTMF.col = COL1[2];
                myDTMF.row = COL2[1];
                break;
            case "9":
                myDTMF.col = COL1[2];
                myDTMF.row = COL2[2];
                break;
            case "C":
                myDTMF.col = COL1[2];
                myDTMF.row = COL2[3];
                break;
            case "*":
                myDTMF.col = COL1[3];
                myDTMF.row = COL2[0];
                break;
            case "0":
                myDTMF.col = COL1[3];
                myDTMF.row = COL2[1];
                break;
            case "#":
                myDTMF.col = COL1[3];
                myDTMF.row = COL2[2];
                break;
            case "D":
                myDTMF.col = COL1[3];
                myDTMF.row = COL2[2];
                break;
            default:
                myDTMF.col = 0;
                myDTMF.row = 0;
        }
        return myDTMF;
    }
}
