package dtmf.dfki.dtmftarsos;

import android.content.Context;
import android.content.Intent;
import android.media.AudioRecord;
import android.util.Log;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ShortBuffer;
import java.util.Calendar;

import be.hogent.tarsos.dsp.pitch.FastYin;
import be.hogent.tarsos.dsp.pitch.PitchDetectionResult;

public class AnalysisThreadRunnable implements Runnable {
	
	/**
	 * ACTION for the broadcast event to add new data to the graph
	 */
	public static final String FREQ_MESSAGE_ACTION = "com.live.frequency.action.freq_broadcast_message_action";

	/**
	 * String indicates the content in the extras of a created broadcast event
	 * The broadcast has here stored the time of an freq event (double)
	 */
	public static final String FREQ_BROADCAST_TIME = "com.live.frequency.broadcastextras.freq_time";

	/**
	 * String indicates the content in the extras of a created broadcast event
	 * The broadcast has here stored the measured/calculated frequency of an
	 * frequency event
	 */
	public static final String FREQ_BROADCAST_FREQUENCY = "com.live.frequency.broadcastextras.freq_itself";

	/**
	 * String indicates the content in the extras of a created broadcast event
	 * The broadcast has here stored the measured/calculated frequency of an
	 * frequency event
	 */
	public static final String FREQ_BROADCASE_PROBABILITY = "com.live.frequency.broadcastextras.freq_probability";

	/**
	 * String indicates the content in the extras of a created broadcast event
	 * The broadcast has here stored here a boolean indicating if the graph
	 * should scroll to the end or not
	 */
	public static final String FREQ_BROADCAST_SCROLL = "com.live.frequency.broadcastextras.freq_scroll_to_graph_end";

	/**
	 * String indicates the content in the extras of a created broadcast event
	 * The broadcast has in this boolean value stored if it is the first
	 * appeared freq event (adds and setup a new graphview)
	 */
	public static final String FREQ_BROADCAST_ISFIRST = "com.live.frequency.broadcastextras.is_first_freq";

	/**
	 * sleeping at starting time. Necessary to skip sound from speech recongintion
	 */
	private static final long START_DELAY = 1000;

	private int bufferSize;
	private AudioRecord recorder;
	private FastYin fastYin;
	private Context context;

	private volatile boolean isStopped;

	public AnalysisThreadRunnable(final AudioRecord recorder, int bufferSize,
                                  Context context) {
		this.recorder = recorder;
		this.bufferSize = bufferSize;
		this.context =context;
	}
	
	@Override
	public void run() {
		try {
			Thread.sleep(START_DELAY);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			Log.e("processBuffer", "Sleeping interrupted: ", e);
			e.printStackTrace();
		}

		recorder.startRecording();
		analysisData();
	}

	private void analysisData() {
		// performed in a thread

		byte data[] = new byte[bufferSize];

		int read = 0;
		int counter = 0;

		while (!this.isStopped()) {
			read = this.recorder.read(data, 0, bufferSize);

			if (AudioRecord.ERROR_INVALID_OPERATION != read) {

				this.processBuffer(data, counter++);
			}
		}

		// finally reset the counter
		counter = 0;

	}

	private boolean isStopped() {
		// TODO read from preferences
		return this.isStopped;
	}

	private void processBuffer(byte[] data, int counter) {
		// execute pitch detection
		Log.d("processBuffer", "Next Buffer full");
		float[] out_buff = new float[this.bufferSize];
		out_buff = this.toFloatArray(data, out_buff);
		this.fastYin = new FastYin(MainActivity.RECORDER_SAMPLERATE, out_buff.length);
		PitchDetectionResult current_pitch = fastYin.getPitch(out_buff);
		current_pitch.getPitch();

		double timeStamp = Calendar.getInstance().getTimeInMillis();
		float pitch = current_pitch.getPitch();
		float probability = current_pitch.getProbability();
		String message = String.format(
                "Pitch detected at %.2fs: %.2fHz ( %.2f probability )\n",
                timeStamp, pitch, probability);
		Log.d("processBuffer", message);

		// send the broadcast event to the recorder thread in order to update the
		// graph
		broadcastFrequency(context,  pitch);

	}

	private float[] toFloatArray(byte[] in_buff, float[] out_buff) {
		ShortBuffer sbuf = ByteBuffer.wrap(in_buff)
				.order(ByteOrder.LITTLE_ENDIAN).asShortBuffer();
		short[] audioShorts = new short[sbuf.capacity()];
		sbuf.get(audioShorts);

		float[] audioFloats = new float[audioShorts.length];
		for (int i = 0; i < audioShorts.length; i++) {
			audioFloats[i] = ((float) audioShorts[i]) / 0x8000;
		}

		return audioFloats;
	}

	public void stop() {
		// stop the recorder and thread
		if (this.recorder != null) {

			this.recorder.stop();
			this.recorder.release();

			this.recorder = null;
		}

		this.setIsStopped(true);
	}
	

	private void setIsStopped(boolean b) {
		// TODO save in preferences for orientation changes etc
		this.isStopped = b;
	}
	
	/**
	 * Notifies UI to update the graph
	 * 
	 * @param context
	 *            application's context

	 * @param pitch
	 *            double the received frequency value for this event. Will be
	 *            stored in the intents extras field FREQ_BROADCAST_FREQUENCY

	 */
	public static void broadcastFrequency(Context context,
			  double pitch) {
		Intent intent = new Intent(FREQ_MESSAGE_ACTION);
 		intent.putExtra(FREQ_BROADCAST_FREQUENCY, pitch);
 		context.sendBroadcast(intent);
	}

}
