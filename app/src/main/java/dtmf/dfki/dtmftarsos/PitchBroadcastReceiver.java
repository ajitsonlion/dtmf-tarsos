package dtmf.dfki.dtmftarsos;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
 import android.widget.TextView;


public class PitchBroadcastReceiver extends BroadcastReceiver {

	private MainActivity parent;


	private double max;
	private double mean;
	private double last;
	private double min;
    TextView textViewNote ;
    TextView textViewFrequency;
    TextView textViewMatchFrequency;

	/**
	 * a counter for all valid values since the last -1. Starts at 0 and goes up
	 * if the detected pitch is >-1 (pitch detected). used for the calculation
	 * of the mean
	 */

	/**
	 * indicates whether the current value is a minus one or not. Used to know
	 * if one wants to update the textfields or not
	 */



	public PitchBroadcastReceiver(MainActivity parent) {
		this.parent = parent;
  		// get the preferences


	}

	@Override
	public void onReceive(Context context, Intent intent) {
		Bundle extras = intent.getExtras();

		double pitch = extras
				.getDouble(AnalysisThreadRunnable.FREQ_BROADCAST_FREQUENCY);

         pitch=Mathematics.round(pitch,0);

        if (pitch>-1){
            Log.d("noteOfPitch", pitch+"");


        }






			}


		}


