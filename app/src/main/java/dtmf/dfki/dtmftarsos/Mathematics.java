package dtmf.dfki.dtmftarsos;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by asingh on 6/18/2014.
 */
public class Mathematics {


    public static float clamp(float val, float min, float max) {
        return Math.max(min, Math.min(max, val));
    }

    public static float norm(float x, float y, float z) {
        return (float) (Math.sqrt(x * x + y * y + z * z));
    }




    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public  static boolean withinRange(double pitch, double frequency,int range){

        double lowerBound= frequency -range;
        double upperBound=frequency+range;
        boolean inRange = Math.abs(pitch - lowerBound) + Math.abs(upperBound - pitch) == Math.abs(upperBound - lowerBound);

        if(inRange){
            return true;

        }

        return   false;


    }
}
